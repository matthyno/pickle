from methods import *
from math import pi as realpi

def decimal_matching(a,b):
	amt_a = len(str(a))-2
	amt_b = len(str(b))-2
	if(amt_a > amt_b):
		amt = amt_b
	elif(amt_a < amt_b):
		amt = amt_a
	elif(amt_a == amt_b):
		amt = amt_b
	else:
		print("A problem occurred")
	if(a == b):
		return amt 
	for i in range(amt+1):
		if((round(a,i) == round(b,i))):
			pass
		elif(not round(a,i) == round(b,i)):
			return i
		else:
			print("A problem occurred")

assert (decimal_matching(3.141592653538, 3.141590932) == 6),"Test #1 failed."
assert (decimal_matching(3.141592653538, 3.141520932) == 4),"Test #2 failed."

print("All OK, for decimal_matching.")

def run_tests_pi(iters):
	grl = grlpi(iters, 1000)
	#am = ampi(iters, 1000)
	nk = nkpi(iters, 1000)
	#gal = galpi(iters, 1000)
	chrs = cpi(iters, 1000)
	#print(str(gal))
	print("Gregory-Leibniz matching digits with Pi: "+str(decimal_matching(grl, realpi)))
	#print("Archimedes matching digits with Pi: "+str(decimal_matching(am, realpi)))
	print("Nilakantha matching digits with Pi: "+str(decimal_matching(nk, realpi)))
	#print("Gauss-Legendre matching digits with Pi: "+str(decimal_matching(gal, realpi)))
	print("Chris matching digits with Pi: "+str(decimal_matching(chrs, realpi)))

run_tests_pi(100000)

